function createNewUser() {
  const newUser = {
    setFirstName(first) {
      return (this.firstName = first);
    },
    setLastName(last) {
      return (this.lastName = last);
    },
    getLogin() {
      return this.firstName[0].toLowerCase() + this.lastName.toLowerCase();
    },

    setBirthday(date) {
      const dateParts = date.split(".");
      return (this.birthday = new Date(
        +dateParts[2],
        +dateParts[1] - 1,
        +dateParts[0]
      ));
    },

    getAge() {
        const currentDate = new Date();
        const currentYear = currentDate.getFullYear();
        return currentYear - this.birthday.getFullYear();
    },

    getPassword() {
      return (
        this.firstName[0].toUpperCase() +
        this.lastName.toLowerCase() +
        this.birthday.getFullYear()
      );
    },
  };
  newUser.setFirstName(prompt("First name").trim());
  newUser.setLastName(prompt("Last name").trim());
  newUser.setBirthday(prompt("Date of birth (dd.mm.yyyy)").trim());

  return newUser;
}

const userNew = createNewUser();

console.log(`Age: ${userNew.getAge()}, password: ${userNew.getPassword()}`);
